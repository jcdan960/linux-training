import os

# for each folder in the current directory

base_dir = "/home/jc/Documents/repo/linux-training/topic/"
def move_stuff():
    print("Moving stuff")

    for folder in os.listdir(base_dir):
        if os.path.isdir(base_dir + folder):
            os.rename(base_dir + folder + "/notes", base_dir + folder + "_notes")
   

def delete_folders():
    print("Deleting folders")
    for folder in os.listdir(base_dir):
        if os.path.isdir(base_dir + folder):
            os.removedirs(base_dir + folder)

if __name__ == "__main__":
    #move_stuff()
    delete_folders()